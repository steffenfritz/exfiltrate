package main

import (
	exfil "codeberg.org/steffenfritz/exfiltrate"
	"flag"
)

func main() {

	icmp4 := flag.Bool("i4", false, "Send message using icmp IPv4")
	msg := flag.String("m", "test", "Message payload to send.")
	dnsA := flag.String("dA", "", "Send message as a DNS request to a fake dns server")
	rcvport := flag.String("p", "53", "Set the receiving port for several tests and where applicable")
	proto := flag.String("proto", "tcp", "Set the protocol for several tests and where applicable")
	flag.Parse()

	if *icmp4 {
		err := exfil.ICMPSendPingv4(*msg, "127.0.0.1")
		if err != nil {
			println(err.Error())
		}
	}

	if len(*dnsA) != 0 {
		dnsConfig := exfil.DNSConfig{
			Address: *dnsA,
			Port:    *rcvport,
			Proto:   *proto,
		}
		exfil.ViaResolver(dnsConfig, *msg)
	}
}
